# przyslowia

**przyslowia** polegają na uzupełnianiu luk w polskich przysłowiach. Utrzymana w terminalowej stylistyce i bardzo minimalistyczna gra, stworzona na potrzeby GameJamu grarantanny.

## Fajne rzeczy

* Baza prawie 1000 polskich przysłów.
* Uczy zarówno przysłów, jak i pisania na klawiaturze.
* Wciągający gameplay.
* Tablica wyników (nie do końca działa ale jest :P)
* Minimalistyczna stylistyka.
* Wersje na Linuxa i Windowsa.
* It's Open Source!

Gra została sklecona na szybko w dwóch ostatnich dobach konkursu, jest więc wczesną wersją tego, co tak naprawdę chciałem zrobić. Ale i tak jest ok :D Zgłaszajcie ewentualne błędy na GitLabie lub tutaj.

## Co jeszcze może się pojawić

Dostosowywanie się do rozmiaru terminala.
Obsługa polskich znaków.
Uładnianie (lecz wciąż w duchu tej samej stylistyki).
Naprawienie leaderboardów.
Bugfixy (meh...)

itch.io: https://grzesiek11.itch.io/przyslowia

## Technikalia i statystyki

* Język: C++
* Użyte biblioteki: ncurses
* Linijki kodu: 354
* Czas poświęcony: w przybliżeniu dwa dni: zacząłem 24 marca. Godzin nie liczyłem.
* Budowanie na Windowsie: popsute, ale MSYS2 pomógł :D